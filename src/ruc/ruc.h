#ifndef _libruc_ruc_h
#define _libruc_ruc_h
#include "roc.h"

/**
 * Calculates net benefit of a predictor for the given threshold probability
 *
 * \param true_pos
 * \param false_pos
 * \param total
 * \param thresh_prob
 */
double net_benefit(unsigned int true_pos, unsigned int false_pos,
                   unsigned int total, double thresh_prob);

/**
 * Calculates the net benefit for a ROC matrix
 *
 * \param mat
 * \param thresh_prob
 */
double ROCMatrix_net_benefit(ROCMatrix *mat, double thresh_prob);

/**
 * Calculates net benefit curve for the given threshold probability range
 *
 * \param true_pos
 * \param false_pos
 * \param total
 * \param thresh_prob
 * \param len
 */
double *net_benefit_curve(unsigned int true_pos, unsigned int false_pos,
                          unsigned int total, double *thresh_prob, size_t len);

/**
 * Calculates net benefit curve for the given threshold probability range
 *
 * \param mat
 * \param thresh_prob
 * \param len
 */
double *ROCMatrix_net_benefit_curve(ROCMatrix *mat, double *thresh_prob,
                                    size_t len);
/**
 *
 *
 * \param tpr
 * \param fpr
 * \param risk_thresh
 * \param prev
 * \param cost
 */
double ruc_risk_ge_prev(double tpr, double fpr,
                        double risk_thresh, double prev, double cost);

/**
 *
 *
 * \param tpr
 * \param fpr
 * \param risk_thresh
 * \param prev
 * \param cost
 */
double ruc_risk_lt_prev(double tpr, double fpr,
                        double risk_thresh, double prev, double cost);

/**
 * Calculates the relative utility curve over a range of risk thresholds
 *
 * \param tpr
 * \param fpr
 * \param risk_thresh_range
 * \param prevalence
 * \param cost
 * \param len
 */
double *relative_utility_curve(double *tpr, double *fpr,
                               double *risk_thresh_range, double prevalence,
                               double cost, size_t len);

/**
 * Calculates relative utility curve over a range of risk thresholds
 *
 * \param arr
 * \param risk_thresh
 * \param prevalence
 * \param cost
 * \param len
 */
double *ROCMatrix_relative_utility_curve(ROCMatrix *roc_arr[],
                                         double *risk_thresh_range,
                                         double prevalence, double cost,
                                         size_t len);
#endif
