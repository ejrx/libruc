#ifndef _libruc_roc_h
#define _libruc_roc_h

#include <stdlib.h>

/* START: ROC General */
/**
 * Comparison functions should return coded 0/1 ad Y/N for outcomes
 */
typedef int (*binary_compare)(double value, double cutpoint);

/**
 * Allocates heap memory for an arbitrary-sized 2d array
 *
 * \param width the width of the array
 * \param height the height of the array
 */
int **allocate_matrix(size_t width, size_t height);

/**
 * Initializes a heap-allocated matrix of 0's of specified size
 *
 */
int **init_zero_matrix(size_t width, size_t height);

/**
 * Frees the memory of a heap-alloc'd matrix of the given height
 *
 */
void free_matrix(int **mat, size_t height);

typedef struct ROCMatrix {
    int true_pos;
    int true_neg;
    int false_pos;
    int false_neg;
} ROCMatrix;

/**
 * Allocates the memory for a new ROCMatrix
 *
 */
ROCMatrix *ROCMatrix_allocate();

/**
 * Populates a ROCMatrix with the specified values
 *
 * \param actual the values from an experiment or observation
 * \param pred the values from your classifier
 * \param len the length of the `actual` and `pred` arrays
 * \param mat pointer to the ROCMatrix struct to populate
 */
void ROCMatrix_populate(int *actual, int *pred, size_t len, ROCMatrix *mat);

/**
 * Frees the heap-alloc'd ROCMatrix struct
 *
 * \param mat pointer to be freed
 */
void ROCMatrix_free(ROCMatrix *mat);

/**
 * Classifies the prediction array using the given cutpoint
 *
 * \param pred prediction values
 * \param size_t the length of the prediction array
 * \param cutpoint the threshold to determine classification with
 * \param compare the function to use to classify (GE, GT, LT, LE)
 */
void classify_binary(double *pred, size_t len, double cutpoint,
                     binary_compare compare, int classified[len]);

/**
 * Creates a ROC curve
 *
 * \param actual the values from an experiment or observation
 * \param pred the predictions from the classifier
 * \param len the length of the
 * \param cutpoints
 * \param compare
 */
double **create_roc_curve(int *actual, double *pred, size_t len,
                          double *cutpoints, size_t len_ctpts,
                          binary_compare compare);

/**
 * \brief free the memory of a heap-alloc'd roc curve
 * \param curve
 * \param len
 */
void free_roc_curve(double **curve, size_t len);
/* END: ROC General */

/* START: ROC Metrics */
/**
 * Calculates sensitivity
 *
 * \param tp true positives
 * \param fn false negatives
 */
double sensitivity_roc(int tp, int fn);
double ROCMatrix_sensitivity(ROCMatrix *mat);

/**
 * Calculates specificity
 *
 * \param tn true negatives
 * \param fp false positives
 */
double specificity_roc(int tn, int fp);
double ROCMatrix_specificity(ROCMatrix *mat);

/**
 * Calculates positive predicitive value
 *
 * \param tp true positives
 * \param fp false positives
 */
double positive_predictive_value_roc(int tp, int fp);
double ROCMatrix_ppv(ROCMatrix *mat);

/**
 * Calculates negative predictive value
 *
 * \param tn true negatives
 * \param fn false negatives
 */
double negative_predictive_value_roc(int tn, int fn);
double ROCMatrix_npv(ROCMatrix *mat);

/**
 * Calculates false positive rate
 *
 * \param tn true negatives
 * \param fp false positives
 */
double fallout_roc(int tn, int fp);
double ROCMatrix_fallout(ROCMatrix *mat);

/**
 * Calculates false discovery rate
 *
 * \param tp true positives
 * \param fp false positives
 */
double false_discovery_rate_roc(int tp, int fp);
double ROCMatrix_fdr(ROCMatrix *mat);

/**
 * Calculates miss rate
 *
 * \param tp true positives
 * \param fn false negatives
 */
double miss_rate_roc(int tp, int fn);
double ROCMatrix_missrate(ROCMatrix *mat);

/**
 * Calculates accuracy
 *
 * \param tp true positives
 * \param tn true negatives
 * \param fp false positives
 * \param fn false negatives
 */
double accuracy_roc(int tp, int tn, int fp, int fn);

double ROCMatrix_accuracy(ROCMatrix *mat);

/**
 * Calculates F1 score (harmonic mean of precision & sensitivity)
 *
 * \param tp true positives
 * \param fp false positives
 * \param fn false negatives
 */
double f1_score_roc(int tp, int fp, int fn);
double ROCMatrix_f1(ROCMatrix *mat);

/**
 * Calculates Matthews correlation coefficient
 *
 * \param tp true positives
 * \param tn true negatives
 * \param fp false positives
 * \param fn false negatives
 */
double matthews_corr_coeff_roc(int tp, int tn, int fp, int fn);
double ROCMatrix_matthews(ROCMatrix *mat);
/* END: ROC Metrics */

/* START: Cutpoint functions */
/**
 * Greater than
 *
 * \param value
 * \param cutpoint
 */
int roc_cut_gt(double value, double cutpoint);

/**
 * Greater than or equal to
 *
 * \param value
 * \param cutpoint
 */
int roc_cut_ge(double value, double cutpoint);

/**
 * Less than
 *
 * \param value
 * \param cutpoint
 */
int roc_cut_lt(double value, double cutpoint);

/**
 * Less than or equal to
 *
 * \param value
 * \param cutpoint
 */
int roc_cut_le(double value, double cutpoint);
/* END: Cutpoint functions */

#define foreach(item, array)                                    \
    for(int keep = 1,                                           \
            count = 0,                                          \
            size = sizeof (array) / sizeof *(array);            \
        keep && count != size;                                  \
        keep = !keep, count ++)                                 \
        for (item = (array) + count; keep; keep = !keep)
#endif
