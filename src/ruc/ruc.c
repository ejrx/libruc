#include "ruc.h"
#include "roc.h"

double net_benefit(unsigned int true_pos, unsigned int false_pos,
                   unsigned int total, double thresh_prob)
{
    double pt_term = thresh_prob/(1 - thresh_prob);
    return (double)true_pos/total + ((double)false_pos/total)*pt_term;
}

double ROCMatrix_net_benefit(ROCMatrix *mat, double thresh_prob)
{
    int total = mat->true_pos + mat->false_pos + mat->true_neg + mat->false_neg;
    return net_benefit(mat->true_pos, mat->false_pos, total, thresh_prob);
}

double *net_benefit_curve(unsigned int true_pos, unsigned int false_pos,
                          unsigned int total, double *thresh_prob, size_t len)
{
    double *nbc = calloc(len, sizeof(double));
    size_t i;
    for (i = 0; i < len; i++) {
        nbc[i] = net_benefit(true_pos, false_pos, total, *(thresh_prob+i));
    }
    return nbc;
}

double *ROCMatrix_net_benefit_curve(ROCMatrix *mat, double *thresh_prob,
                                    size_t len)
{
    double *nbc = calloc(len, sizeof(double));
    size_t i;
    for (i = 0; i < len; i++) {
        nbc[i] = ROCMatrix_net_benefit(mat, *(thresh_prob+i));
    }
    return nbc;
}

double ruc_risk_ge_prev(double tpr, double fpr,
                              double risk_thresh, double prev, double cost)
{
    return tpr - ((1-prev)/prev)*(risk_thresh/(1-risk_thresh))*fpr - cost/prev;
}

double ruc_risk_lt_prev(double tpr, double fpr,
                        double risk_thresh, double prev, double cost)
{
    return (1-fpr)-(1-tpr)*(prev/(1-prev))*((1-risk_thresh)/risk_thresh) - ((1-risk_thresh)/risk_thresh)*(cost/(1-prev));
}

double *relative_utility_curve(double *tpr, double *fpr,
                               double *risk_thresh_range, double prevalence,
                               double cost, size_t len)
{
    double *ruc = calloc(len, sizeof(double));
    size_t i;
    for (i = 0; i < len; i++) {
        if (*(risk_thresh_range+i) < prevalence) {
            ruc[i] = ruc_risk_lt_prev(tpr[i], fpr[i], risk_thresh_range[i],
                                      prevalence, cost);
        } else {
            ruc[i] = ruc_risk_ge_prev(tpr[i], fpr[i], risk_thresh_range[i],
                                      prevalence, cost);
        }
    }
    return ruc;
}

double *ROCMatrix_relative_utility_curve(ROCMatrix *roc_arr[],
                                         double *risk_thresh_range,
                                         double prevalence, double cost,
                                         size_t len)
{
    double *ruc = calloc(len, sizeof(double));
    size_t i;
    for (i = 0; i < len; i++) {
        //calculate tpr and fpr
        int total = roc_arr[i]->true_pos + roc_arr[i]->true_neg + roc_arr[i]->false_pos + roc_arr[i]->false_neg;
        if (*(risk_thresh_range+i) < prevalence) {
            ruc[i] = ruc_risk_lt_prev(roc_arr[i]->true_pos/total,
                                      roc_arr[i]->false_pos/total,
                                      *(risk_thresh_range+i),
                                      prevalence, cost);
        } else {
            ruc[i] = ruc_risk_ge_prev(roc_arr[i]->true_pos/total,
                                      roc_arr[i]->false_pos/total,
                                      *(risk_thresh_range+i),
                                      prevalence, cost);
        }
    }
    return ruc;
}
