#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "dbg.h"
#include "roc.h"

/* START: ROC General */

static const ROCMatrix ZeroROC = { 0, 0, 0, 0 };

int **allocate_matrix(size_t width, size_t height)
{
    int **mat;
    mat = malloc(height * sizeof *mat);
    size_t i;
    for (i = 0; i < height; ++i) {
        mat[i] = malloc(width * sizeof *mat[i]);
    }
    return mat;
}

int **init_zero_matrix(size_t width, size_t height)
{
    int **mat;
    mat = malloc(height * sizeof *mat);
    size_t i;
    for (i = 0; i < height; ++i) {
        mat[i] = calloc(width, sizeof *mat[i]);
    }
    return mat;
}

void free_matrix(int **mat, size_t height)
{
    size_t i;
    for (i = 0; i < height; i++) {
        free(mat[i]);  //free each row
    }
    free(mat);
}

ROCMatrix *ROCMatrix_allocate()
{
    ROCMatrix *mat = calloc(1, sizeof(ROCMatrix));
    check_mem(mat);

    return mat;
 error:
    if (mat) {
        ROCMatrix_free(mat);
    }
    return NULL;
}

void ROCMatrix_populate(int *actual, int *pred, size_t len, ROCMatrix *mat)
{
    size_t i;
    for (i = 0; i < len; i++) {
        if (*(actual + i) == 0) {  //condition negative
            if (*(pred + i) == 0) {  //test negative
                mat->true_neg++;
            } else {  //test positive
                mat->false_pos++;
            }
        } else {  //condition positive
            if (*(pred +i) == 0) {  //test negative
                mat->false_neg++;
            } else {  //test positive
                mat->true_pos++;
            }
        }
    }
}

void ROCMatrix_free(ROCMatrix *mat)
{
    if (mat) {
        free(mat);
    }
}

void classify_binary(double *pred, size_t len, double cutpoint,
                     binary_compare compare, int classified[len])
{
    size_t i;
    for (i = 0; i < len; i++) {
        classified[i] = compare(pred[i], cutpoint);
    }
}

double **create_roc_curve(int *actual, double *pred, size_t len,
                          double *cutpoints, size_t len_ctpts,
                          binary_compare compare)
{
    //allocate the len_ctpts x 2 array
    double **curve;
    curve = malloc(len_ctpts * sizeof *curve);  //1 row per cutpoint
    size_t i;
    for (i = 0; i < len_ctpts; ++i) {
        curve[i] = malloc(2 * sizeof *curve[i]);
    }

    //allocate ROCMatrix
    ROCMatrix *roc_mat = ROCMatrix_allocate();
    int j = 0;  //count variable for cutpoints

    int pred_classified[len];  //classified predictors
    memset(pred_classified, 0, sizeof(pred_classified));  //init to 0

    foreach(double *c, cutpoints) {
        *roc_mat = ZeroROC;//reset the ROCMatrix to 0
        //run the classifier for the given cutpoint
        classify_binary(pred, len, *c, compare, pred_classified);

        //populate the ROCMatrix according to the classified values
        ROCMatrix_populate(actual, pred_classified, len, roc_mat);
        curve[j][0] = ROCMatrix_sensitivity(roc_mat);
        curve[j][1] = ROCMatrix_fallout(roc_mat);
        j++;  //increment
    }
    ROCMatrix_free(roc_mat);
    return curve;
}

void free_roc_curve(double **roc, size_t len)
{
    size_t i;
    for (i = 0; i < len; i++) {
        free(roc[i]);  //free each row
    }
    free(roc);
}
/* END: ROC General */

/* START: ROC Metrics */
double sensitivity_roc(int tp, int fn)
{
    return (double)tp/(tp + fn);
}
double ROCMatrix_sensitivity(ROCMatrix *mat)
{
    return sensitivity_roc(mat->true_pos, mat->false_neg);
}

double specificity_roc(int tn, int fp)
{
    return (double)tn/(tn + fp);
}
double ROCMatrix_specificity(ROCMatrix *mat)
{
    return specificity_roc(mat->true_neg, mat->false_pos);
}

double positive_predictive_value_roc(int tp, int fp)
{
    return (double)tp/(tp + fp);
}
double ROCMatrix_ppv(ROCMatrix *mat)
{
    return positive_predictive_value_roc(mat->true_pos, mat->false_pos);
}

double negative_predictive_value_roc(int tn, int fn)
{
    return (double)tn/(tn + fn);
}
double ROCMatrix_npv(ROCMatrix *mat)
{
    return negative_predictive_value_roc(mat->true_neg, mat->false_neg);
}

double fallout_roc(int tn, int fp)
{
    return (double)fp/(fp + tn);
}
double ROCMatrix_fallout(ROCMatrix *mat)
{
    return fallout_roc(mat->true_neg, mat->false_pos);
}

double false_discovery_rate_roc(int tp, int fp)
{
    return (double)fp/(fp + tp);
}
double ROCMatrix_fdr(ROCMatrix *mat)
{
    return false_discovery_rate_roc(mat->true_pos, mat->false_pos);
}

double miss_rate_roc(int tp, int fn)
{
    return (double)fn/(fn + tp);
}
double ROCMatrix_missrate(ROCMatrix *mat)
{
    return miss_rate_roc(mat->true_pos, mat->false_neg);
}

double accuracy_roc(int tp, int tn, int fp, int fn)
{
    return (double)(tp + tn)/(tp + tn + fp + fn);
}
double ROCMatrix_accuracy(ROCMatrix *mat)
{
    return accuracy_roc(mat->true_pos, mat->true_neg, mat->false_pos,
                        mat->false_neg);
}

double f1_score_roc(int tp, int fp, int fn)
{
    double _twotp = (double)2*tp;
    return _twotp/(_twotp + fp + fn);
}
double ROCMatrix_f1(ROCMatrix *mat)
{
    return f1_score_roc(mat->true_pos, mat->false_pos, mat->false_neg);
}

double matthews_corr_coeff_roc(int tp, int tn, int fp, int fn)
{
    int total_pos = tp + fp;
    int total_neg = tn + fn;
    int numer = tp * tn - fp * fn;
    double denom = sqrt(total_pos * (tp+fn) * (tn+fp) * total_neg);

    return numer/denom;
}
double ROCMatrix_matthews(ROCMatrix *mat)
{
    return matthews_corr_coeff_roc(mat->true_pos, mat->true_neg,
                                   mat->false_pos, mat->false_neg);
}
/* END: ROC Metrics */

/* START: Cutpoint functions */
int roc_cut_gt(double value, double cutpoint)
{
    if (value > cutpoint) {
        return 1;
    } else {
        return 0;
    }
}

int roc_cut_ge(double value, double cutpoint)
{
    if (value >= cutpoint) {
        return 1;
    } else {
        return 0;
    }
}

int roc_cut_lt(double value, double cutpoint)
{
    if (value < cutpoint) {
        return 1;
    } else {
        return 0;
    }
}

int roc_cut_le(double value, double cutpoint)
{
    if (value <= cutpoint) {
        return 1;
    } else {
        return 0;
    }
}
/* END: Cutpoint functions */
