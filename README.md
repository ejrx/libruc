#libruc
A lightweight C library for measuring binary classification performance.

## Supported metrics
**Reciever operating characteristic (ROC) curves**

**Relative utility curves**
Originally described by Baker, et. al. in [Using relative utility curves to evaluate risk prediction](http://www.ncbi.nlm.nih.gov/pubmed/20069131).

**Decision analysis curves**
Originally described by Vickers in [Decision curve analysis: a novel method for evaluating prediction models](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2577036/).

## Installation
Clone and build.

    git clone https://github.com/ejrx/libruc
    cd libruc
    make install

## License
This work is licensed under the [GNU General Public License v3](http://www.gnu.org/copyleft/gpl.html).
