#include <ruc/roc.h>
#include <ruc/ruc.h>
#include "minunit.h"

int true_pos = 63;
int false_pos = 28;
int true_neg = 72;
int false_neg = 37;
int total = 200;

double eps = 0.0001; /* tunes acceptability threshold for comparisons */

ROCMatrix *init_rocmat()
{
    ROCMatrix *mat = ROCMatrix_allocate();
    mat->true_pos = true_pos;
    mat->true_neg = true_neg;
    mat->false_pos = false_pos;
    mat->false_neg = false_neg;
    return mat;
}

char *test_net_benefit()
{
    double nb = net_benefit(true_pos, false_pos, total, 0.33);
    return NULL;
}

char *test_ROCMatrix_net_benefit()
{
    ROCMatrix *mat = init_rocmat();
    double nb = ROCMatrix_net_benefit(mat, 0.33);
    ROCMatrix_free(mat);
    return NULL;
}

char *test_ruc_risk_lt_prev()
{
    return NULL;
}

char *test_ruc_risk_ge_prev()
{
    return NULL;
}

char *test_net_benefit_curve()
{
    double thresh_range[6] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6};
    double *nbc = net_benefit_curve(true_pos, false_pos, total,
        thresh_range, 6);
    free(nbc);
    return NULL;
}

char *test_ROCMatrix_net_benefit_curve()
{
    ROCMatrix *mat = init_rocmat();
    double thresh_range[6] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6};
    double *nbc = ROCMatrix_net_benefit_curve(mat, thresh_range, 6);
    ROCMatrix_free(mat);
    free(nbc);
    return NULL;
}

char *all_tests()
{
    mu_suite_start();
    /* START: list tests */
    mu_run_test(test_net_benefit);
    mu_run_test(test_ROCMatrix_net_benefit);
    mu_run_test(test_ruc_risk_lt_prev);
    mu_run_test(test_ruc_risk_ge_prev);
    mu_run_test(test_net_benefit_curve);
    mu_run_test(test_ROCMatrix_net_benefit_curve);
    /* END: list tests */
    return NULL;
}

RUN_TESTS(all_tests);
