#include <stdlib.h>
#include <math.h>
#include <ruc/roc.h>
#include "minunit.h"

/* Taken from the Wikipedia page on ROC */
int true_pos = 63;
int false_pos = 28;
int true_neg = 72;
int false_neg = 37;

double eps = 0.0001; /* tunes acceptability threshold for comparisons */

int **conf_mat;

ROCMatrix *default_roc()
{
    ROCMatrix *mat = ROCMatrix_allocate();
    mat->true_pos = true_pos;
    mat->false_pos = false_pos;
    mat->true_neg = true_neg;
    mat->false_neg = false_neg;
    return mat;
}

int compare(double expected, double actual, double epsilon)
{
    if (fabs(expected - actual) <= epsilon*fabs(expected)) {
        return 0;
    } else {
        return 1;
    }
}

int cut_gt(double value, double cutpoint)
{
    if (value > cutpoint) {
        return 1;
    } else {
        return 0;
    }
}

ROCMatrix *roc_mat = NULL;

char *test_ROCMatrix_allocate()
{
    roc_mat = ROCMatrix_allocate();
    mu_assert(roc_mat != NULL, "ROCMatrix allocation failed");
    return NULL;
}

char *test_ROCMatrix_populate()
{
    int actual[10] = {0,1,1,0,1,0,0,1,1,1};
    int pred[10] =   {0,0,1,0,1,1,1,0,1,1};
    ROCMatrix_populate(actual, pred, 10, roc_mat);

    /* compare ROCMatrix values to expected */
    mu_assert(roc_mat->true_pos == 4, "roc_mat tp failed");
    mu_assert(roc_mat->true_neg == 2, "roc_mat tn failed");
    mu_assert(roc_mat->false_pos == 2, "roc_mat fp failed");
    mu_assert(roc_mat->false_neg == 2, "roc_mat fn failed");
    return NULL;
}

char *test_ROCMatrix_free()
{
    ROCMatrix_free(roc_mat);
    return NULL;
}

char *test_classify_binary()
{
    double pred[5] = {0.1, 0.2, 0.3, 0.4, 0.5};
    size_t len = 5;
    int (*comp)(double,double);
    comp = &cut_gt;
    int expected[5] = {0, 0, 1, 1, 1};
    int actual[5] = { 0 };

    classify_binary(pred, len, 0.25, comp, actual);

    size_t i;
    for (i = 0; i < len; i++) {
        mu_assert(*(actual+i) == *(expected+i), "binary classifier failed");
    }

    return NULL;
}

char *test_create_roc_curve()
{
    int actual[10] = {0,0,1,1,0,1,1,0,0,0};
    double pred[10] = {0.25, 0.55, 0.75, 0.42, 0.5, 0.9, 0.67, 0.00, 0.4, 0.2};
    double ctpt[10] = {0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65};
    int (*comp)(double,double);
    comp = &cut_gt;

    double **roc = create_roc_curve(actual, pred, 10, ctpt, 10, comp);
    mu_assert(roc != NULL, "ROC create failed");
    return NULL;
}

ROCMatrix *meas_roc;
char *test_sensitivity()
{
    double expected = (double)63/100;
    double actual = sensitivity_roc(true_pos, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "sensitivity failed");
    return NULL;
}
char *test_ROCMatrix_sensitivity()
{
    meas_roc = default_roc();
    double actual = ROCMatrix_sensitivity(meas_roc);
    double expected = sensitivity_roc(true_pos, false_neg);
    mu_assert(actual == expected, "ROC sensitivity failed");
    return NULL;
}
char *test_specificity()
{
    double expected = (double)72/100;
    double actual = specificity_roc(true_neg, false_pos);
    mu_assert(compare(expected, actual, eps) == 0, "specificity failed");
    return NULL;
}
char *test_ROCMatrix_specificity()
{
    double actual = ROCMatrix_specificity(meas_roc);
    double expected = specificity_roc(true_neg, false_pos);
    mu_assert(actual == expected, "ROC specificity failed");
    return NULL;
}

char *test_ppv()
{
    double expected = (double)63/91;
    double actual = positive_predictive_value_roc(true_pos, false_pos);
    mu_assert(compare(expected, actual, eps) == 0, "ppv failed");
    return NULL;
}
char *test_ROCMatrix_ppv()
{
    double expected = positive_predictive_value_roc(true_pos, false_pos);
    double actual = ROCMatrix_ppv(meas_roc);
    mu_assert(actual == expected, "ROC ppv failed");
    return NULL;
}

char *test_npv()
{
    double expected = (double)72/109;
    double actual = negative_predictive_value_roc(true_neg, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "npv failed");
    return NULL;
}
char *test_ROCMatrix_npv()
{
    double expected = negative_predictive_value_roc(true_neg, false_neg);
    double actual = ROCMatrix_npv(meas_roc);
    mu_assert(actual == expected, "ROC npv failed");
    return NULL;
}

char *test_fallout()
{
    double expected = (double)28/100;
    double actual = fallout_roc(true_neg, false_pos);
    mu_assert(compare(expected, actual, eps) == 0, "fallout failed");
    return NULL;
}
char *test_ROCMatrix_fallout()
{
    double actual = ROCMatrix_fallout(meas_roc);
    double expected = fallout_roc(true_neg, false_pos);
    mu_assert(actual == expected, "ROC fallout failed");
    return NULL;
}

char *test_false_discovery_rate()
{
    double expected = (double)28/91;
    double actual = false_discovery_rate_roc(true_pos, false_pos);
    mu_assert(compare(expected, actual, eps) == 0, "false discovery rate failed");
    return NULL;
}
char *test_ROCMatrix_fdr()
{
    double expected = false_discovery_rate_roc(true_pos, false_pos);
    double actual = ROCMatrix_fdr(meas_roc);
    mu_assert(actual == expected, "ROC false discovery rate failed");
    return NULL;
}

char *test_miss_rate()
{
    double expected = (double)37/100;
    double actual = miss_rate_roc(true_pos, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "miss rate failed");
    return NULL;
}
char *test_ROCMatrix_missrate()
{
    double expected = miss_rate_roc(true_pos, false_neg);
    double actual = ROCMatrix_missrate(meas_roc);
    mu_assert(actual == expected, "ROC miss rate failed");
    return NULL;
}

char *test_accuracy()
{
    double expected = (double)(63+72)/200;
    double actual = accuracy_roc(true_pos, true_neg, false_pos, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "accuracy failed");
    return NULL;
}
char *test_ROCMatrix_accuracy()
{
    double expected = accuracy_roc(true_pos, true_neg, false_pos, false_neg);
    double actual = ROCMatrix_accuracy(meas_roc);
    mu_assert(actual == expected, "ROC accuracy failed");
    ROCMatrix_free(meas_roc);
    return NULL;
}

char *test_f1()
{
    double expected = (double)2*true_pos/(2*true_pos + false_pos + false_neg);
    double actual = f1_score_roc(true_pos, false_pos, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "F1 score failed");
    return NULL;
}
char *test_ROCMatrix_f1()
{
    double expected = f1_score_roc(true_pos, false_pos, false_neg);
    double actual = ROCMatrix_f1(meas_roc);
    mu_assert(actual == expected, "ROC F1 failed");
    return NULL;
}

char *test_matthews_corr_coeff()
{
    double denom = sqrt((true_pos + false_pos)*(true_pos+false_neg)*(true_neg+false_pos)*(true_neg+false_neg));
    double expected = ((true_pos*true_neg)-(false_pos*false_neg))/denom;
    double actual = matthews_corr_coeff_roc(true_pos, true_neg, false_pos, false_neg);
    mu_assert(compare(expected, actual, eps) == 0, "matthews coeff failed");
    return NULL;
}
char *test_ROCMatrix_matthews()
{
    double expected = matthews_corr_coeff_roc(true_pos, true_neg,
                                              false_pos, false_neg);
    double actual = ROCMatrix_matthews(meas_roc);
    mu_assert(actual == expected, "ROC Matthews failed");
    return NULL;
}

char *test_alloc_free_matrix()
{
    int **matrix;
    matrix = allocate_matrix(2,2);
    mu_assert(matrix != NULL, "Failed to create matrix");
    free_matrix(matrix, 2);
    return NULL;
}

char *test_init_zero_matrix()
{
    int **cmat = init_zero_matrix(2, 2);
    int i, j;
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            mu_assert(cmat[i][j] == 0, "failed calloc");
        }
    }
    free_matrix(cmat, 2);
    return NULL;
}

char *test_foreach()
{
    int i = 1;
    int values[5] = {1, 2, 3, 4, 5};
    foreach(int *v, values) {
        mu_assert(*v == i, "foreach failed");
        i++;
    }
    return NULL;
}

char *all_tests()
{
    mu_suite_start();
    /* START: list tests */
    mu_run_test(test_classify_binary);
    mu_run_test(test_ROCMatrix_allocate);
    mu_run_test(test_ROCMatrix_populate);
    mu_run_test(test_ROCMatrix_free);
    mu_run_test(test_create_roc_curve);
    mu_run_test(test_sensitivity);
    mu_run_test(test_ROCMatrix_sensitivity);
    mu_run_test(test_specificity);
    mu_run_test(test_ROCMatrix_specificity);
    mu_run_test(test_ppv);
    mu_run_test(test_ROCMatrix_ppv);
    mu_run_test(test_npv);
    mu_run_test(test_ROCMatrix_npv);
    mu_run_test(test_miss_rate);
    mu_run_test(test_ROCMatrix_missrate);
    mu_run_test(test_matthews_corr_coeff);
    mu_run_test(test_ROCMatrix_matthews);
    mu_run_test(test_false_discovery_rate);
    mu_run_test(test_ROCMatrix_fdr);
    mu_run_test(test_fallout);
    mu_run_test(test_ROCMatrix_fallout);
    mu_run_test(test_f1);
    mu_run_test(test_ROCMatrix_f1);
    mu_run_test(test_accuracy);
    mu_run_test(test_ROCMatrix_accuracy);
    mu_run_test(test_alloc_free_matrix);
    mu_run_test(test_init_zero_matrix);
    mu_run_test(test_foreach);
    /* END: list tests */
    return NULL;
}

RUN_TESTS(all_tests);
